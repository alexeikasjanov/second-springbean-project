package com.javastart.springbeans.service;


public class HelloXmlService {

    private UtilService utilService;

    public void setUtilService(UtilService utilService) {
        this.utilService = utilService;
    }

    public String  hello(){

        return utilService.append("Hello from Xml!");
    }
}
