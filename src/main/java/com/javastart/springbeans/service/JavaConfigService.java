package com.javastart.springbeans.service;

public class JavaConfigService {

    private final String timeOffInitialization;

    private Integer dayOfMounth;

    public JavaConfigService(String timeOffInitialization) {
        this.timeOffInitialization = timeOffInitialization;
    }

    public void setDayOfMounth(Integer dayOfMounth) {
        this.dayOfMounth = dayOfMounth;
    }

    public String hello(){
        return "Hello from JavaConfig!" + " time: "+
                timeOffInitialization + " dayOfMounth: " + dayOfMounth;
    }
}
